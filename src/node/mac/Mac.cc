#include "Mac.h"

Define_Module(Mac);

void Mac::initialize() {

    // Get local node endpoint id
    this->eid_ = this->getParentModule()->getIndex();

    // Store total neighbors
    this->neighbors_ = this->getParentModule()->getVectorSize();

    // Store a pointer to local mobility module
    mobilityModule = (Mobility *) this->getParentModule()->getSubmodule(
            "mobility");

    // Init max range
    maxRange = par("maxRange").doubleValue();

    // Init reachability matrix and contact start time:
    for (int eid = 0; eid < neighbors_; eid++) {
        reachabilityMatrix.push_back(false);
        contactStartTime.push_back(-1);
    }

    // Init contact plan recording
    if (par("record").boolValue()) {

        stringstream fileName;
        fileName << "contacts_" << this->eid_ << ".txt";
        recordFile.open(fileName.str());
    }

    // Schedule poll for neighbor proximity
    cMessage * msg = new cMessage();
    // Set scheduling priority (smaller numeric value is prioritized)
    // so that mac is allways updated after mobility.
    msg->setSchedulingPriority(1);
    scheduleAt(simTime() + 1, msg);
}

void Mac::handleMessage(cMessage *msg) {

    if (msg->arrivedOn("gateToNetData$i")) {

        //////////////////////////////
        // New bundle from Net
        //////////////////////////////

        Bundle* bundle = check_and_cast<Bundle *>(msg);

        // Get next hop EiD
        int destinationEid = bundle->getNextHopEid();
        cModule *destinationModule =
                this->getParentModule()->getParentModule()->getSubmodule("node",
                        destinationEid)->getSubmodule("mac");

        // Forward to next hop
        sendDirect(msg, destinationModule, "gateToAir");

    } else if (msg->arrivedOn("gateToAir")) {

        //////////////////////////////
        // New bundle from Air-int.
        //////////////////////////////

        // TODO: if you would like to do error checking or bundle
        // discard by channel errors, or request retransmission
        // at the Mac level this would be the place.

        // Send to Net
        send(msg, "gateToNetData$o");

    } else if (msg->isSelfMessage()) {

        //////////////////////////////
        // Neighbor reachability poll
        //////////////////////////////

        // Update reachabilityMatrix
        if (this->reachabilityPoll()) {

            // Let Net know if it has changed
            send(new cMessage, "gateToNetControl$o");
        }

        // Schedule next poll
        scheduleAt(simTime() + 1, msg);
    }
}

bool Mac::reachabilityPoll() {

    bool matrixChanged = false;

    // Go though all neighbor nodes
    for (int eid = 0; eid < neighbors_; eid++) {

        double distance = mobilityModule->getDistanceToNode(eid);
        if (distance < maxRange && eid != eid_) {

            // if node was not reachable, matrix changed
            if (reachabilityMatrix[eid] == false) {
                matrixChanged = true;

                // record start of new contact
                if (par("record").boolValue())
                    contactStartTime[eid] = simTime().dbl();
            }

            // this is a rechable node
            reachabilityMatrix[eid] = true;
        } else {

            // if node was reachable, matrix changed
            if (reachabilityMatrix[eid] == true) {
                matrixChanged = true;

                // record end of new contact
                if (par("record").boolValue()) {
                    recordFile << "a contact +" << contactStartTime[eid] << " +"
                            << simTime().dbl() << " " << eid_ << " " << eid
                            << " " << "100" << endl;
                }
            }

            // this is not a reachable node
            reachabilityMatrix[eid] = false;
        }
    }

    return matrixChanged;
}

void Mac::finish() {

    if (par("record").boolValue()) {

        // Go though all neighbor nodes
        for (int eid = 0; eid < neighbors_; eid++) {

            // if node is reachable, end and record contact
            if (reachabilityMatrix[eid] == true) {
                recordFile << "a contact +" << contactStartTime[eid] << " +"
                        << simTime().dbl() << " " << eid_ << " " << eid << " "
                        << "100" << endl;
            }
        }
        // close file
        recordFile.close();

        // Concatenate files
        if (eid_ == 0) {

            ofstream contacPlanFile("contactPlan.txt");

            // Go though all neighbor contact files
            for (int eid = 0; eid < neighbors_; eid++) {

                stringstream fileName;
                fileName << "contacts_" << eid << ".txt";
                ifstream contactFile(fileName.str());

                // drop all contacts in contactPlan
                contacPlanFile << contactFile.rdbuf();
            }
            contacPlanFile.close();
        }
    }

//    std::ifstream file1( "file1.txt" ) ;
//    std::ifstream file2( "file2.txt" ) ;
//    std::ifstream file3( "file3.txt" ) ;
//    std::ofstream combined_file( "combined_file.txt" ) ;
//    combined_file << file1.rdbuf() << file2.rdbuf() << file3.rdbuf() ;

}

Mac::Mac() {

}

Mac::~Mac() {

}

