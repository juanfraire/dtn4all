#ifndef MAC_H_
#define MAC_H_

#include <vector>
#include <omnetpp.h>

#include <Mobility.h>
#include <dtn4all_m.h>

using namespace std;
using namespace omnetpp;

class Mac: public cSimpleModule {
public:
    Mac();
    virtual ~Mac();

    // Reachability matrix (accessed from Net)
    vector<bool> reachabilityMatrix;

    // Max Range (accessed from Graphics)
    double maxRange;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *);
    virtual void finish();

private:
    // scan reachable neighbors and return if matrix changed
    bool reachabilityPoll();

    // Local node endpoint id
    int eid_;

    // Number of neighbors
    int neighbors_;

    // Pointer to mobility module
    Mobility *mobilityModule;

    // Contact plan record file
    ofstream recordFile;

    // Contact start time (used for contact plan record)
    vector<double> contactStartTime;

};

#endif /* MAC_H_ */
