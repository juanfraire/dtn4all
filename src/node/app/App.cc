#include "App.h"

Define_Module(App);

void App::initialize() {

    // Store a pointer to local node module
    nodeModule = this->getParentModule();

    // Get local node endpoint id
    this->eid_ = nodeModule->getIndex();

    // Send messages (if any)
    generateNewMessages(par("messages").longValue());

    // Schedule next generation of messages
    if (par("sendInterval").doubleValue() > 0)
        scheduleAt(simTime() + par("sendInterval").doubleValue() , new cMessage);


}
void App::handleMessage(cMessage *msg) {

    if (msg->arrivedOn("gateToNet$i")) {

        // New bundle arrived from Net
        Bundle* bundle = check_and_cast<Bundle *>(msg);

        if (!receivedIDs.checkIfBundleRegistered(bundle)) {
            receivedIDs.registerBundle(bundle);
            if (!checkIfMessageExpired(bundle)) {
                rxCnt++;
                EV << bundle->getName() << " Bundle from node " << bundle->getSourceEID() << " received after  " << simTime().dbl() - bundle->getCreationTime().dbl() << " seconds" << endl;
                EV << bundle->getName() << " Bundle from node " << bundle->getSourceEID() << " received with max waiting time of  " << bundle->getMaxWaitingTime().dbl() << " seconds" << endl;
                if( bundle->getApplicationAcknowledgmentRequest() == true) {
                    acknowledgeMessage(bundle);
                }
            }

        }
     } else { // self-message
         // Send messages (if any)
         generateNewMessages(par("messages").longValue());

         // Schedule next generation of messages
         scheduleAt(simTime() + par("sendInterval").doubleValue() , new cMessage);

     }

    // Nothing else to do with this bundle
    delete (msg);
}

void App::generateNewMessages(int total) {

    int cnt = 0;
    while(cnt < total) {
        // Create bundle and send it to Net
         Bundle* bundle = new Bundle("ApplicationDATA");
         bundle->setSourceEID(this->eid_);
         bundle->setNextHopEid(this->eid_);
         bundle->setDestinationEID(par("destinationEID"));
         bundle->setCreationTimestamp(simTime());
         bundle->setSequenceNumber(cnt);
         bundle->setLifetime(par("lifetime").longValue());
         bundle->setHopCount(0);
         bundle->setHopLimit(par("hoplimit").longValue());
         bundle->setApplicationAcknowledgmentRequest(par("applicationAcknowledgmentRequest").boolValue());
         bundle->setByteLength(par("byteSize").longValue() );
         bundle->setMaxWaitingTime(0);
         send(bundle, "gateToNet$o");
         cnt++;
    }
 }

void App::acknowledgeMessage(Bundle *bundle) {

    static int seq = 0;

    Bundle* ack = new Bundle("ApplicationACK");
    ack->setSourceEID(this->eid_);
    //ack->setNextHopEid(this->eid_);
    ack->setDestinationEID(bundle->getSourceEID());
    ack->setCreationTimestamp(simTime());
    ack->setSequenceNumber(seq++);
    ack->setLifetime(par("lifetime").longValue());
    ack->setHopCount(0);
    ack->setHopLimit(par("hoplimit").longValue());
    ack->setApplicationAcknowledgmentRequest(false);
    send(ack, "gateToNet$o");
}

bool App::checkIfMessageExpired(Bundle *bundle) {

        simtime_t deliveryTime = simTime() - bundle->getCreationTime();
        if (deliveryTime > bundle->getLifetime()) {
            char text[32];
            sprintf(text, "Received Bundle has expired after %.0f seconds", bundle->getLifetime().dbl());
            nodeModule->bubble(text);
            return true;
        }
        else
            return false;
}

void App::refreshDisplay() const
{
    char buf[40];
    sprintf(buf, "rx: %d", rxCnt);
    getDisplayString().setTagArg("t", 0, buf);
}

void App::finish() {

    EV << "# Messages received: " << rxCnt << endl;
}

App::App() {

}

App::~App() {

}
