#ifndef APP_H_
#define APP_H_

#include <omnetpp.h>
#include <Record.h>

#include <dtn4all_m.h>

using namespace omnetpp;
using namespace std;

class App: public cSimpleModule {
public:
    App();
    virtual ~App();

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *);
    virtual void refreshDisplay() const;
    virtual void finish();

private:
    // Local node endpoint id
    int eid_;

    // Received messages counter
    int rxCnt=0;

    // Pointer to node
    cModule *nodeModule;

    void generateNewMessages(int total);
    bool checkIfMessageExpired(Bundle *bundle);
    void acknowledgeMessage(Bundle *bundle);

    Record receivedIDs;

};

#endif /* APP_H_ */
