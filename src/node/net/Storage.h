#ifndef STORAGE_H_
#define STORAGE_H_

#include <map>
#include <queue>
#include <omnetpp.h>
#include <dtn4all_m.h>

using namespace std;

class Storage {
public:
    Storage();
    virtual ~Storage();

    int storedMsg;

    int getNumberOfStoredMessages() const;

    // Erase all storage
    virtual void freeStorage();

    ///////////////////
    // Store:
    ///////////////////

    // Enqueue bundle to queue
    virtual void enqueueBundleToQueue(Bundle * bundle, int queueId);

    ///////////////////
    // Read and erase:
    ///////////////////

    // 1) Is bundle in queue?
    virtual bool isQueueEmpty(int queueId);

    // 2) Get next bundle in queue (does not erase it)
    virtual Bundle * getNextBundleInQueue(int queueId);

    // 3) Pop next bundle in queue
    virtual void popNextBundleInQueue(int queueId);

private:

    // Map of queues
    map<int, queue<Bundle *> > bundlesQueue_;
};

#endif /* STORAGE_H_ */
