#include <Storage.h>

void Storage::enqueueBundleToQueue(Bundle * bundle, int queueId) {
    // Check is queue exits, if not, create it.
    map<int, queue<Bundle *> >::iterator it = bundlesQueue_.find(queueId);

    if (it != bundlesQueue_.end()) {
        it->second.push(bundle);
    } else {
        queue<Bundle *> q;
        q.push(bundle);
        bundlesQueue_[queueId] = q;
    }
    storedMsg++;
}

bool Storage::isQueueEmpty(int queueId) {
    // This functions returns false if there is a queue
    // with bundles for the queueId. If it is empty
    // or not-existant, the function returns true

    map<int, queue<Bundle *> >::iterator it = bundlesQueue_.find(queueId);

    if (it != bundlesQueue_.end())
        return (bundlesQueue_[queueId].empty());
    return true;
}

Bundle * Storage::getNextBundleInQueue(int queueId) {
    map<int, queue<Bundle *> >::iterator it = bundlesQueue_.find(queueId);

    // Just check if the function was called incorrectly
    if (it == bundlesQueue_.end())
        if (bundlesQueue_[queueId].empty()) {
            cout << "***getNextBundleInQueue called but queue empty***" << endl;
            exit(1);
        }

    // Find and return pointer to bundle
    queue<Bundle *> bundlesToTx = it->second;

    return bundlesToTx.front();
}

void Storage::popNextBundleInQueue(int queueId) {
    // Pop the next bundle for this contact
    map<int, queue<Bundle *> >::iterator it = bundlesQueue_.find(queueId);
    queue<Bundle *> bundlesToTx = it->second;

    bundlesToTx.pop();

    // Update queue after popping the bundle
    if (!bundlesToTx.empty())
        bundlesQueue_[queueId] = bundlesToTx;
    else
        bundlesQueue_.erase(queueId);

    storedMsg--;
}

int Storage::getNumberOfStoredMessages() const {
    return storedMsg;
}

void Storage::freeStorage() {
    // Delete all enqueued bundles
    map<int, queue<Bundle *> >::iterator it1 = bundlesQueue_.begin();
    map<int, queue<Bundle *> >::iterator it2 = bundlesQueue_.end();
    while (it1 != it2) {
        int bundlesDeleted = 0;

        queue<Bundle *> bundles = it1->second;

        while (!bundles.empty()) {
            delete (bundles.front());
            bundles.pop();
            bundlesDeleted++;
        }

        bundlesQueue_.erase(it1++);
    }
    storedMsg = 0;
}

Storage::Storage() {
    storedMsg = 0;
}

Storage::~Storage() {
}

