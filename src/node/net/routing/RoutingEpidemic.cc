/*
 * RoutingEpidemic.cc
 *
 *  Created on: Apr 4, 2017
 *      Author: jfinochietto
 */

#include <RoutingEpidemic.h>

RoutingEpidemic::RoutingEpidemic(int nodes, int nodeIndex) {
    numNodes = nodes;
    nodeId = nodeIndex;

}

RoutingEpidemic::~RoutingEpidemic() {
}

vector<int> RoutingEpidemic::routeBundle(Bundle * bundle, double simTime) {
    // Return destination EID for each network node different than the current node
    vector<int> nextHops;
    for (int n = 0; n < numNodes; n++)
        if (n != nodeId)
            nextHops.push_back(n);
    return nextHops;
}
