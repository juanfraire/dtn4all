/*
 * RoutingCgr.h
 *
 *  Created on: Apr 4, 2017
 *      Author: juanfraire
 */

#ifndef ROUTING_ROUTINGCGR_H_
#define ROUTING_ROUTINGCGR_H_

#include "Routing.h"
#include "ContactPlan.h"
#include <fstream>

class RoutingSch: public Routing {
public:
    RoutingSch(int nodes, int nodeIndex);
    virtual ~RoutingSch();

    // Overide functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);

private:
    int numNodes;
    int nodeId;

    ContactPlan contactPlan_;

    void parseContacts(string fileName);
};

#endif /* ROUTING_ROUTINGCGR_H_ */
