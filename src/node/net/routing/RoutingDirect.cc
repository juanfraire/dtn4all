/*
 * RoutingDirect.cpp
 *
 *  Created on: Apr 4, 2017
 *      Author: juanfraire
 */

#include <routing/RoutingDirect.h>

RoutingDirect::RoutingDirect() {
}

RoutingDirect::~RoutingDirect() {
}

vector<int> RoutingDirect::routeBundle(Bundle * bundle, double simTime) {
    // Return destination EID (direct forwarding)
    vector<int> nextHops;
    nextHops.push_back(bundle->getDestinationEID());
    return nextHops;
}
