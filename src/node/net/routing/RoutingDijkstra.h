
#ifndef ROUTING_ROUTINGCGR350_H_
#define ROUTING_ROUTINGCGR350_H_

#define MAX_SPEED_MPH   (150000)

#include "Routing.h"
#include "ContactPlan.h"
#include <fstream>
#include <limits>

class RoutingDijkstra: public Routing {
public:
    RoutingDijkstra(int nodes, int nodeIndex);
    virtual ~RoutingDijkstra();

    // Overide functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);

private:
    int numNodes;
    int nodeId;

    ContactPlan contactPlan_;

    void parseContacts(string fileName);

    typedef struct
    {
        int toNodeNbr;  //Initial-hop neighbor.
        double fromTime;    // init tx time
        double toTime;      // Time at which route shuts down: earliest contact end time among all
        float arrivalConfidence;
        double arrivalTime;
        double maxCapacity; // in Bits
        vector<Contact *> hops; // list: IonCXref addr
    } CgrRoute;

    typedef struct
    {
        Contact * contact;
        Contact * predecessor;  // predecessor Contact
        double arrivalTime;
        double capacity;    // in Bytes (?)
        bool visited;
        bool suppressed;
    } Work;

};

#endif /* ROUTING_ROUTINGCGR350_H_ */
