/*
 * RoutingDirect.h
 *
 *  Created on: Apr 4, 2017
 *      Author: juanfraire
 */

#ifndef ROUTING_ROUTINGDIRECT_H_
#define ROUTING_ROUTINGDIRECT_H_

#include "Routing.h"

class RoutingDirect : public Routing
{
public:
    RoutingDirect();
    virtual ~RoutingDirect();

    // Override functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);
};

#endif /* ROUTING_ROUTINGDIRECT_H_ */
