#ifndef CONTACT_H_
#define CONTACT_H_

#include<iostream>

using namespace std;

class Contact {
public:

    Contact(int id, double start, double end, int sourceEid, int destinationEid,
            double dataRate, float confidence);
    virtual ~Contact();

    // Important functions for practice
    int getSourceEid() const;
    int getDestinationEid() const;
    double getStart() const;
    double getEnd() const;

    // Other functions
    double getDataRate() const;
    int getId() const;
    double getResidualCapacity() const;
    double getDuration() const;
    float getConfidence() const;
    void setResidualCapacity(double residualCapacity);

    // A pointer to any external data
    void * work;

private:

    int id_;
    double start_;
    double end_;
    int sourceEid_;
    int destinationEid_;
    double dataRate_;
    double residualCapacity_;
    float confidence_;
};

#endif /* CONTACT_H_ */
