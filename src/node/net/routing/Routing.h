/*
 * Routing.h
 *
 *  Created on: Apr 4, 2017
 *      Author: juanfraire
 */

#ifndef ROUTING_H_
#define ROUTING_H_

#include "dtn4all_m.h"
#include "Storage.h"

using namespace omnetpp;
using namespace std;

class Routing
{
public:
    Routing()
    {
    }
    virtual ~Routing()
    {
    }
    // This is the a pure virtual method that return
    // the queue ID to forward a bundle.(it need to be
    // overriden by specific routing classes).
    virtual vector<int> routeBundle(Bundle *bundle, double simTime) = 0;
};

#endif /* ROUTING_H_ */
