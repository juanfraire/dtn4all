/*
 * RoutingGeo.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jfinochietto
 */

#include <RoutingGeo.h>

RoutingGeo::RoutingGeo(int nodes, int nodeIndex) {
    numNodes = nodes;
    nodeId = nodeIndex;
}

RoutingGeo::~RoutingGeo() {
}

vector<int> RoutingGeo::routeBundle(Bundle * bundle, double simTime) {
    // Return destination EID (direct forwarding)
    vector<int> nextHops;
    if (nodeId == 0)
        nextHops.push_back(nodeId + 1); // Down
    else if (nodeId == numNodes - 1)
        nextHops.push_back(nodeId - 1); // Up
    else if (bundle->getDestinationEID() > nodeId)
        nextHops.push_back(nodeId + 1); // Down
    else
        nextHops.push_back(nodeId - 1); // Up

    return nextHops;
}
