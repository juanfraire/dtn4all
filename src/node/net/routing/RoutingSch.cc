/*
 * RoutingCgr.cc
 *
 *  Created on: Apr 4, 2017
 *      Author: juanfraire
 */

#include <RoutingSch.h>

RoutingSch::RoutingSch(int nodes, int nodeIndex) {

    numNodes = nodes;
    nodeId = nodeIndex;

    // Load contact plan file
    this->parseContacts("contactPlan.txt");
}

RoutingSch::~RoutingSch() {
}

vector<int> RoutingSch::routeBundle(Bundle * bundle, double simTime) {

    vector<int> nextHops;

    ////////////////////////////////////////////////////////
    // Use Contacts and Contact Plan (contactPlan_)
    // functions to decide which is the best next hop
    ////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////
    // This version is a two-hop exploration of the contact plan
    ////////////////////////////////////////////////////////

    vector<tuple<int, double> > routeList;

    vector<Contact> directContact1;
    vector<Contact> directContact2;

    // Check if a direct contact exists
    directContact1 = contactPlan_.getFirstContactBySrcDst(nodeId,
            bundle->getDestinationEID(), simTime);
    if (!directContact1.empty()) {
        routeList.push_back(
                tuple<int, double>(directContact1.at(0).getDestinationEid(),
                        directContact1.at(0).getStart()));
    }

    // Check if two hop contact exists
    for (int n = 0; n < numNodes; n++) {
        if (n != nodeId) {
            directContact1 = contactPlan_.getFirstContactBySrcDst(nodeId, n,
                    simTime);

            // if directContact 1 is not empty
            if (!directContact1.empty()) {

                // Check if next hop can reach destination
                directContact2 = contactPlan_.getFirstContactBySrcDst(n,
                        bundle->getDestinationEID(), simTime);
                if (!directContact2.empty()) {

                    // We can reach destination, note the route
                    routeList.push_back(
                            tuple<int, double>(
                                    directContact1.at(0).getDestinationEid(),
                                    directContact2.at(0).getStart()
                                            > directContact1.at(0).getStart() ?
                                            directContact2.at(0).getStart() :
                                            directContact1.at(0).getStart()));
                }
            }
        }
    }

    // Evaluate routeList and choose next best hop
    int nextHop = -1;
    double bestDeliveryTime = 10000;
    for (size_t i = 0; i < routeList.size(); i++) {

        if (get < 1 > (routeList.at(i)) < bestDeliveryTime) {
            bestDeliveryTime = get < 1 > (routeList.at(i));
            nextHop = get < 0 > (routeList.at(i));
        }
        cout << "in node " << nodeId << " route: " << get < 0
                > (routeList.at(i)) << " " << get < 1
                > (routeList.at(i)) << endl;
    }

    if (nextHop != -1) {
        nextHops.push_back(nextHop);
        cout << "in node " << nodeId << " nextHop: " << nextHop << endl;
    } else {
        // Do nothing, there is no path to destination in contact plan...
        cout << "in node " << nodeId << " no nextHop found" << endl;
    }

    return nextHops;
}

void RoutingSch::parseContacts(string fileName) {
    int id = 1;
    double start = 0.0;
    double end = 0.0;
    int sourceEid = 0;
    int destinationEid = 0;
    double dataRate = 0.0;

    string aux = "#";
    string a;
    string command;
    ifstream file;
    file.open(fileName.c_str());

    if (!file.is_open())
        throw cException(
                ("Error: wrong path to contacts file " + string(fileName)).c_str());

    while (true) {
        if (aux.empty())
            getline(file, aux, '\n');
        else if (aux.at(0) == '#')
            getline(file, aux, '\n');
        else
            break;
    }

    stringstream ss(aux);
    ss >> a >> command >> start >> end >> sourceEid >> destinationEid
            >> dataRate;

    do {
        if ((command.compare("contact") == 0)) {
            contactPlan_.addContact(id, start, end, sourceEid, destinationEid,
                    dataRate, (float) 1.0);

            id++;
        }
    } while (file >> a >> command >> start >> end >> sourceEid >> destinationEid
            >> dataRate);

    file.close();

    contactPlan_.setContactsFile(fileName);
    contactPlan_.finishContactPlan();
}
