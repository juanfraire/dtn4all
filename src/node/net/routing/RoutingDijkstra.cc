/*
 * RoutingCgrIon350.cpp
 *
 *  Created on: Apr 6, 2017
 *      Author: juanfraire
 */

#include <RoutingDijkstra.h>

RoutingDijkstra::RoutingDijkstra(int nodes, int nodeIndex) {

    numNodes = nodes;
    nodeId = nodeIndex;

    // Load contact plan file
    this->parseContacts("contactPlan.txt");
}

RoutingDijkstra::~RoutingDijkstra() {
}

vector<int> RoutingDijkstra::routeBundle(Bundle * bundle, double simTime) {

    // Create rootContact and its corresponding rootWork
    Contact rootContact(0, 0, 0, nodeId, nodeId, 0, 1.0);
    Work rootWork;
    rootWork.contact = &rootContact;
    rootWork.arrivalTime = simTime;
    rootContact.work = &rootWork;

    // Create and initialize a working area in each contact
    for (vector<Contact>::iterator it = contactPlan_.getContacts()->begin();
            it != contactPlan_.getContacts()->end(); ++it) {
        (*it).work = new Work;
        ((Work *) (*it).work)->contact = &(*it);
        ((Work *) (*it).work)->arrivalTime = numeric_limits<double>::max();
        ((Work *) (*it).work)->predecessor = 0;
        ((Work *) (*it).work)->visited = false;
    }

    // Start Dijkstra
    Contact * currentContact = &rootContact;
    Contact * finalContact = NULL;
    double earliestFinalArrivalTime = numeric_limits<double>::max();

    while (1) {
        // Go thorugh all next hop neighbors in the
        // contact plan (all contacts which source
        // node is the currentWork destination node)

        vector<Contact> currentNeighbors = contactPlan_.getContactsBySrc(
                currentContact->getDestinationEid());

        for (vector<Contact>::iterator it = currentNeighbors.begin();
                it != currentNeighbors.end(); ++it) {
            // First, check if contact needs to be considered
            // in ion an if (contact->fromNode > arg.fromNode)
            // is necesary due to the red-black tree stuff. Not here :)

            // This contact is finished, ignore it.
            if ((*it).getEnd()
                    <= ((Work *) (currentContact->work))->arrivalTime)
                continue;

            // This contact is suppressed/visited, ignore it.
            if (((Work *) (*it).work)->suppressed
                    || ((Work *) (*it).work)->visited)
                continue;

            // Calculate the cost for this contact (Arrival Time)
            double arrivalTime;
            if ((*it).getStart()
                    < ((Work *) (currentContact->work))->arrivalTime)
                arrivalTime = ((Work *) (currentContact->work))->arrivalTime;
            else
                arrivalTime = (*it).getStart();

            // Update the cost of this contact
            if (arrivalTime < ((Work *) (*it).work)->arrivalTime) {
                ((Work *) (*it).work)->arrivalTime = arrivalTime;
                ((Work *) (*it).work)->predecessor = currentContact;

                // If this contact reaches the terminus node
                if ((*it).getDestinationEid() == bundle->getDestinationEID()) {

                    // consider it as final contact
                    if (((Work *) (*it).work)->arrivalTime
                            < earliestFinalArrivalTime) {
                        earliestFinalArrivalTime =
                                ((Work *) (*it).work)->arrivalTime;
                        // Warning: we need to point finalContact to
                        // the real contact in contactPlan. This iteration
                        // goes over a copy of the original contact plan
                        // returned by getContactsBySrc().
                        finalContact = contactPlan_.getContactById(
                                (*it).getId());
                    }
                }
            }
        } // end for (currentNeighbors)

        ((Work *) (currentContact->work))->visited = true;

        // Select next (best) contact to move to in next iteration
        Contact * nextContact = NULL;
        double earliestArrivalTime = numeric_limits<double>::max();
        for (vector<Contact>::iterator it = contactPlan_.getContacts()->begin();
                it != contactPlan_.getContacts()->end(); ++it) {
            // Do not evaluate suppressed or visited contacts
            if (((Work *) (*it).work)->suppressed
                    || ((Work *) (*it).work)->visited)
                continue;

            // If the arrival time is worst than the best found so far, ignore
            if (((Work *) (*it).work)->arrivalTime > earliestFinalArrivalTime)
                continue;

            // Then this might be the best candidate contact
            if (((Work *) (*it).work)->arrivalTime < earliestArrivalTime) {
                nextContact = &(*it);
                earliestArrivalTime = ((Work *) (*it).work)->arrivalTime;
            }
        }
        if (nextContact == NULL)
            break; // No next contact found, exit search

        // Update next contact and go with next itartion
        currentContact = nextContact;

    } // end while (1)

    vector<int> nextHops;

    // If we got a final contact to destination
    // then it is the best route and we need to
    // translate the info from the work area to
    // the CgrRoute route pointer.
    if (finalContact != NULL) {

        CgrRoute * route = new CgrRoute;

        route->arrivalTime = earliestFinalArrivalTime;

        double earliestEndTime = numeric_limits<double>::max();

        // Go through all contacts in the path
        for (Contact * contact = finalContact; contact != &rootContact;
                contact = ((Work *) (*contact).work)->predecessor) {

            // Get earliest end time
            if (contact->getEnd() < earliestEndTime)
                earliestEndTime = contact->getEnd();

            // Store hop:
            route->hops.insert(route->hops.begin(), contact);
        }

        route->toNodeNbr = route->hops[0]->getDestinationEid();
        route->fromTime = route->hops[0]->getStart();
        route->toTime = earliestEndTime;

        cout << "node:" << nodeId << " route found to "
                << bundle->getDestinationEID() << " through node:"
                << route->toNodeNbr << ", arrivalT:" << route->arrivalTime
                << ", txWin:(" << route->fromTime << "-" << route->toTime << ")"
                << endl;

        nextHops.push_back(route->toNodeNbr);
    } else {
        // no route found
        cout << "node:" << nodeId << " route not found to "
                << bundle->getDestinationEID() << endl;
    }

    return nextHops;
}

void RoutingDijkstra::parseContacts(string fileName) {
    int id = 1;
    double start = 0.0;
    double end = 0.0;
    int sourceEid = 0;
    int destinationEid = 0;
    double dataRate = 0.0;

    string aux = "#";
    string a;
    string command;
    ifstream file;
    file.open(fileName.c_str());

    if (!file.is_open())
        throw cException(
                ("Error: wrong path to contacts file " + string(fileName)).c_str());

    while (true) {
        if (aux.empty())
            getline(file, aux, '\n');
        else if (aux.at(0) == '#')
            getline(file, aux, '\n');
        else
            break;
    }

    stringstream ss(aux);
    ss >> a >> command >> start >> end >> sourceEid >> destinationEid
            >> dataRate;

    do {
        if ((command.compare("contact") == 0)) {
            contactPlan_.addContact(id, start, end, sourceEid, destinationEid,
                    dataRate, (float) 1.0);

            id++;
        }
    } while (file >> a >> command >> start >> end >> sourceEid >> destinationEid
            >> dataRate);

    file.close();

    contactPlan_.setContactsFile(fileName);
    contactPlan_.finishContactPlan();
}
