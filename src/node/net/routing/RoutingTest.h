/*
 * RoutingTest.h
 *
 *  Created on: Nov 9, 2019
 *      Author: juanfraire
 */

#ifndef SRC_NODE_NET_ROUTING_ROUTINGTEST_H_
#define SRC_NODE_NET_ROUTING_ROUTINGTEST_H_

#include "Routing.h"

class RoutingTest : public Routing {

public:
    RoutingTest(int nodes, int nodeIndex);
    virtual ~RoutingTest();

    // Override functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);

private:
    int numNodes;
    int nodeId;
};

#endif /* SRC_NODE_NET_ROUTING_ROUTINGTEST_H_ */
