/*
 * RoutingEpidemic.h
 *
 *  Created on: Apr 4, 2017
 *      Author: jfinochietto
 */

#ifndef NODE_NET_ROUTING_ROUTINGEPIDEMIC_H_
#define NODE_NET_ROUTING_ROUTINGEPIDEMIC_H_

#include "Routing.h"

class RoutingEpidemic : public Routing {

public:
    RoutingEpidemic(int nodes, int nodeIndex);
    virtual ~RoutingEpidemic();

    // Override functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);

private:
    int numNodes;
    int nodeId;
};

#endif /* NODE_NET_ROUTING_ROUTINGEPIDEMIC_H_ */
