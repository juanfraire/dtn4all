/*
 * RoutingGeo.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jfinochietto
 */

#ifndef NODE_NET_ROUTING_ROUTINGGEO_H_
#define NODE_NET_ROUTING_ROUTINGGEO_H_

#include "Routing.h"

class RoutingGeo : public Routing {
public:
    RoutingGeo(int nodes, int nodeIndex);;
    virtual ~RoutingGeo();

    // Override functions
    virtual vector<int> routeBundle(Bundle *bundle, double simTime);

private:
    int numNodes;
    int nodeId;

};

#endif /* NODE_NET_ROUTING_ROUTINGGEO_H_ */
