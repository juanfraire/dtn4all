#ifndef _NET_H_
#define _NET_H_

#include <cstdio>
#include <string>
#include <set>
#include <tuple>

#include <omnetpp.h>
#include <dtn4all_m.h>

#include "Routing.h"
#include "RoutingDirect.h"
#include "RoutingEpidemic.h"
#include "RoutingTest.h"
#include "RoutingGeo.h"
#include "RoutingSch.h"
#include <Mac.h>
#include <Storage.h>
#include <Record.h>
#include <RoutingDijkstra.h>
#include <RoutingSch.h>


using namespace omnetpp;
using namespace std;

class Net: public cSimpleModule {
public:
    Net();
    virtual ~Net();

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void refreshDisplay() const;
    virtual void finish();

    // Enqueue bundle in the correct queue
    void dispatchBundle(Bundle *bundle);

    bool checkIfMessageExpired(Bundle *bundle);


private:

    // Pointer to node
    cModule *nodeModule;

    // Local node endpoint id
    int eid_;

    // Local routing class
    Routing * routing;

    // Pointer to reachability matrix (Mac).
    // The reachability matrix has one entry per possible
    // neigbor. A true means node is reachable, a false
    // means the opposite. The Max sends a meesage to Net
    // then this matrix is updated.
    vector<bool> * reachabilityMatrix;

    // Node storage:
    Storage storage;
    vector<Record> storedIDs; // One record per queue
};

#endif /* NET_H_ */

