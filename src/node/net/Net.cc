#include "Net.h"

Define_Module(Net);

void Net::initialize() {

    // Store a pointer to local node module
    nodeModule = this->getParentModule();

    // Get local node endpoint id
    this->eid_ = nodeModule->getIndex();

    // Setup one Bundle ID register per queue / node
    storedIDs.resize(nodeModule->getVectorSize());

    // Init routing interface
    string routingType = par("routing");
    if (routingType.compare("direct") == 0)
        routing = new RoutingDirect();
    else if (routingType.compare("epidemic") == 0)
        routing = new RoutingEpidemic(nodeModule->getVectorSize(), this->eid_);
    else if (routingType.compare("geo") == 0)
        routing = new RoutingGeo(nodeModule->getVectorSize(), this->eid_);
    else if (routingType.compare("scheduled") == 0)
        routing = new RoutingSch(nodeModule->getVectorSize(), this->eid_);
    else if (routingType.compare("dijkstra") == 0)
        routing = new RoutingDijkstra(nodeModule->getVectorSize(), this->eid_);
    else if (routingType.compare("test") == 0)
        routing = new RoutingTest(nodeModule->getVectorSize(), this->eid_);
    else {
        // Check routing has been started ok
        cout << "Dtn4all error: cannot find routing " << routingType << endl;
        exit(1);
    }

    // Store a pointer to the reachability matrix in Mac
    reachabilityMatrix =
            &(((Mac *) this->getParentModule()->getSubmodule("mac"))->reachabilityMatrix);
}

void Net::handleMessage(cMessage * msg) {

// Process incoming msg
    if (msg->arrivedOn("gateToApp$i")) {

        //////////////////////////////
        // New bundle from App
        //////////////////////////////

        Bundle* bundle = check_and_cast<Bundle *>(msg);

        // Store the bundle in local storage
        this->dispatchBundle(bundle);

    } else if (msg->arrivedOn("gateToMacData$i")) {

        //////////////////////////////
        // New bundle from Mac
        //////////////////////////////

        Bundle* bundle = check_and_cast<Bundle *>(msg);

        if (bundle->getDestinationEID() == this->eid_) {

            // If I am the destination, send to App
            send(bundle, "gateToApp$o");
        } else {

            // If not, route the bundle
            this->dispatchBundle(bundle);
        }
    } else if (msg->arrivedOn("gateToMacControl$i") || msg->isSelfMessage()) {

        //////////////////////////////
        // Update from Mac (reachability matrix changed)
        // or from new bundle dispatched and need to empty queues
        //////////////////////////////

        // TODO: forwarding phase: the first fun part of the course!
        // This portion of the code should implement a scheme to
        // start or finish the transmission of bundles queued
        // in the storage.

        // Go through all nodes in the matrix
        for (unsigned int eid = 0; eid < reachabilityMatrix->size(); eid++) {
            if (reachabilityMatrix->at(eid) == true) {
                // We can reach this eid, check if bundles exists for this eid
                while (!storage.isQueueEmpty(eid)) {
                    Bundle *bundle = storage.getNextBundleInQueue(eid);
                    // Update queue
                    storage.popNextBundleInQueue(eid);

                    // Update previous node
                    bundle->setPreviousNode(this->eid_);
                    // Update hop count
                    bundle->setHopCount(bundle->getHopCount() + 1);

                    // Check if bundle has either expired or exceeded hop count
                    if (bundle->getHopCount() <= bundle->getHopLimit()
                            && !checkIfMessageExpired(bundle)) {
                        // Send bundle
                        send(bundle, "gateToMacData$o");
                        // Update max waiting time metric
                        if ( (simTime() - bundle->getStoredTime()) > bundle->getMaxWaitingTime())
                            bundle->setMaxWaitingTime(simTime() - bundle->getStoredTime());
                    }
                    else {
                        // Delete bundle
                        delete(bundle);
                        EV << "Bundle deleted due to expiration or hop count"
                                  << endl;
                    }
                }
            }
        }

         delete msg;
    }
}

void Net::dispatchBundle(Bundle * bundle) {

    // TODO: routing phase: the fun part of the course!
    // This function should implement the required intelligency
    // to determine the correct queue to store the bundle.
    // For this example, we will enqueue the bundle in a queue
    // with the id of the destination. When there is a direct
    // reachability, we will directly transmit the bundle.

    // TBC: Check if there are already expired bundles in the queue, if so delete them

    // Get next hops to where the bundle need to be forwarded
    vector<int> nextHopEids = routing->routeBundle(bundle, simTime().dbl());

    for (vector<int>::iterator queueId = nextHopEids.begin();
            queueId != nextHopEids.end(); ++queueId) {
        if (!storedIDs[*queueId].checkIfBundleRegistered(bundle)) {

            storedIDs[*queueId].registerBundle(bundle);
            Bundle *bundleCopy = (Bundle *) bundle->dup();
            bundleCopy->setNextHopEid(*queueId);
            // Store the bundle with destination as queueId
            storage.enqueueBundleToQueue(bundleCopy,
                    bundleCopy->getNextHopEid());
            bundleCopy->setStoredTime(simTime());
        } else
            EV << "Bundle already registered in queue " << *queueId
                      << "-> Deleting this bundle" << endl;
    }
    delete bundle; // delete original bundle

    // Schedule update message to check if we can forward this bundle
    scheduleAt(simTime(), new cMessage());

}

bool Net::checkIfMessageExpired(Bundle *bundle) {

    simtime_t deliveryTime = simTime() - bundle->getCreationTime();
    if (deliveryTime > bundle->getLifetime()) {
        char text[32];
        sprintf(text, "Rx bundle expired after %.0f seconds",
                bundle->getLifetime().dbl());
        nodeModule->bubble(text);
        return true;
    } else
        return false;
}

void Net::refreshDisplay() const {
    char buf[40];
    sprintf(buf, "#msg: %d", storage.getNumberOfStoredMessages());
    getDisplayString().setTagArg("t", 0, buf);
}

void Net::finish() {

}

Net::Net() {

    this->storage.freeStorage();

}

Net::~Net() {

}

