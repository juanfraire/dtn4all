/*
 * Record.cpp
 *
 *  Created on: Apr 3, 2017
 *      Author: jfinochietto
 */

#include <Record.h>

Record::Record() {
}

Record::~Record() {
}

void Record::registerBundle(Bundle *bundle) {

    tuple<simtime_t, int, int> id = make_tuple(bundle->getCreationTime(),
            bundle->getSequenceNumber(), bundle->getSourceEID());
    bundleIDs.insert(id);
}

bool Record::checkIfBundleRegistered(Bundle *bundle) {

    tuple<simtime_t, int, int> id = make_tuple(bundle->getCreationTime(),
            bundle->getSequenceNumber(), bundle->getSourceEID());
    set<tuple<simtime_t, int, int>>::iterator it = bundleIDs.find(id);
    if (it != bundleIDs.end())
        return true;
    else
        return false;
}

void Record::unregisterBundle(Bundle *bundle) {

    tuple<simtime_t, int, int> id = make_tuple(bundle->getCreationTime(),
            bundle->getSequenceNumber(), bundle->getSourceEID());
    bundleIDs.erase(id);
}

