#include <Graphics.h>

Define_Module(Graphics);

void Graphics::initialize() {

    // Store this node eid
    this->eid_ = this->getParentModule()->getIndex();
    // Store total neighbors
    this->neighbors_ = this->getParentModule()->getVectorSize();

    // Store a pointer to local node module
    nodeModule = this->getParentModule();

    // Store a pointer to network canvas
    networkCanvas = nodeModule->getParentModule()->getCanvas();

    // Store a pointer to mac module
    macModule = (Mac *) nodeModule->getSubmodule("mac");

    // Store a pointer to local mobility module
    mobilityModule = (Mobility *) nodeModule->getSubmodule("mobility");

    // Init linesVector
    for (int eid = 0; eid < neighbors_; eid++) {
        cLineFigure * line = new cLineFigure("line1");
        line->setLineWidth(1);
        line->setEndArrowhead(cFigure::ARROW_BARBED);
        linesVector.push_back(line);
    }

    // Init radio circle
    circleRadio = new cOvalFigure("circle");
    circleRadio->setLineWidth(1);
    circleRadio->setLineStyle(cFigure::LINE_DOTTED);

    if (par("enable").boolValue()) {
        networkCanvas->addFigure(circleRadio);
    }
}

void Graphics::refreshDisplay() const {

    // Set node position (calculatd in mobility)
    nodeModule->getDisplayString().setTagArg("p", 0, mobilityModule->posX);
    nodeModule->getDisplayString().setTagArg("p", 1, mobilityModule->posY);

    // Turn off connectivity indicator (green icon)
    nodeModule->getDisplayString().setTagArg("i2", 0, "");

    // Turn off enabled connectivity lines
    for (int eid = 0; eid < neighbors_; eid++)
        if (networkCanvas->findFigure(linesVector[eid]) != -1)
            networkCanvas->removeFigure(linesVector[eid]);

    // Go through all neighbors
    for (int eid = 0; eid < neighbors_; eid++) {

        if (macModule->reachabilityMatrix[eid] == true) {

            // Enable connectivity icon
            nodeModule->getDisplayString().setTagArg("i2", 0, "old/x_green");

            // Get remote mobility module
            Mobility *remoteNodeMobility =
                    (Mobility*) nodeModule->getParentModule()->getSubmodule(
                            "node", eid)->getSubmodule("mobility");

            // Draw line between the two connected nodes
            linesVector[eid]->setStart(
                    cFigure::Point(mobilityModule->posX, mobilityModule->posY));
            linesVector[eid]->setEnd(
                    cFigure::Point(remoteNodeMobility->posX,
                            remoteNodeMobility->posY));
            networkCanvas->addFigure(linesVector[eid]);
        }
    }

    // Update radio line position
    circleRadio->setBounds(
            cFigure::Rectangle(mobilityModule->posX - macModule->maxRange,
                    mobilityModule->posY - macModule->maxRange,
                    2 * macModule->maxRange, 2 * macModule->maxRange));

}

void Graphics::finish() {

    // Removal of lines need to happen in class destructor.
    // It seems that another refreshDisplay is called after
    // the Omnetpp calls this finish function. Thus, lines
    // are re-added to the canvas and triggers an error in
    // the end of the simulations.

}

Graphics::Graphics() {

}

Graphics::~Graphics() {

    // Remove connectivity lines
    for (int eid = 0; eid < neighbors_; eid++)
        if (networkCanvas->findFigure(linesVector[eid]) != -1) {
            networkCanvas->removeFigure(linesVector[eid]);
        }

    // Remove circle
    networkCanvas->removeFigure(circleRadio);

    // Delete lines
    for (int eid = 0; eid < neighbors_; eid++) {
        delete linesVector[eid];
    }

    // Delete circle
    delete circleRadio;
}

