//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef GRAPHICS_H_
#define GRAPHICS_H_

#include <omnetpp.h>
#include <vector>

#include <Mobility.h>
#include <Mac.h>
#include <dtn4all_m.h>

class Graphics: public cSimpleModule {

public:
    Graphics();
    virtual ~Graphics();

protected:
    virtual void initialize() override;
    virtual void refreshDisplay() const override;
    virtual void finish() override;

private:

    //void updateGraphics();

    // Local endpoint id
    int eid_;

    // Number of neighbors
    int neighbors_;

    // Pointer to parent canvas
    cCanvas *networkCanvas;

    // Pointer to node
    cModule *nodeModule;

    // Pointer to mobility module
    Mobility *mobilityModule;

    // Pointer to mac
    Mac *macModule;

    // Connectivity lines
    vector<cLineFigure *> linesVector;

    // Radio circle
    cOvalFigure * circleRadio;

};

#endif /* GRAPHICS_H_ */
