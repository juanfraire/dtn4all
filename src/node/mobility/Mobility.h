#ifndef MOBILITY_H_
#define MOBILITY_H_

#include <stdio.h>
#include <string.h>
#include <sstream>
#include <omnetpp.h>
#include <fstream>

using namespace omnetpp;
using namespace std;

class Mobility: public cSimpleModule {

public:
    Mobility();
    virtual ~Mobility();

    // Get distance to remote node (accessed by Mac)
    double getDistanceToNode(int eid);
    double getDistanceToTarget(double, double);

    // Current position (accessed by graphics)
    double posX;
    double posY;

protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *);
    virtual void refreshDisplay() const;
    virtual void finish();

    // Calculate random target
    virtual void getNewTarget();

private:

    // Local node endpoint id
    int eid_;

    // Target points and speed
    double targetX;
    double targetY;
    double speed;
    double lastDistance;

    // Increments until reaching target
    double incrementX;
    double incrementY;

    // Pointer to node
    cModule *nodeModule;

    // Record file
    ofstream recordFile;
    ifstream mobiltyFile;
};

#endif
