#include <mobility/Mobility.h>
#include <cmath>
using namespace std;

Define_Module(Mobility);

void Mobility::initialize() {

    // Store a pointer to local node module
    nodeModule = this->getParentModule();

    // Get local node endpoint id
    this->eid_ = this->getParentModule()->getIndex();

    // Initial x,y and speed values
    string mobilityType = par("type");
    if (mobilityType.compare("scheduled") == 0) {

        // Get them from file
        stringstream fileName;
        fileName << "mobilityRecord_" << this->eid_ << ".txt";
        mobiltyFile.open(fileName.str());

        mobiltyFile >> posX >> posY >> speed;

    } else if (mobilityType.compare("predicted") == 0) {

        // Calculate them
        posX = uniform(par("areaMinX").doubleValue(),
                par("areaMaxX").doubleValue());
        posY = (double) this->getParentModule()->getIndex() * 1000;

        // Set speed
        speed = uniform(par("minSpeed").doubleValue(),
                par("maxSpeed").doubleValue());

    } else { // Random

        // Calculate them
        posX = uniform(par("areaMinX").doubleValue(),
                par("areaMaxX").doubleValue());
        posY = uniform(par("areaMinY").doubleValue(),
                par("areaMaxY").doubleValue());

        // Set speed
        speed = uniform(par("minSpeed").doubleValue(),
                par("maxSpeed").doubleValue());
    }

    // Init mobility recording
    if (par("record").boolValue()) {

        // Init record file
        stringstream fileName;
        fileName << "mobilityRecord_" << this->eid_ << ".txt";
        recordFile.open(fileName.str());

        // Write initial target and speed to file
        recordFile << posX << " " << posY << " " << speed << endl;
    }

    // Get a target point
    this->getNewTarget();

    // Compute distance to target
    lastDistance = this->getDistanceToTarget(targetX, targetY);

    WATCH(posY);

    // Schedule update message
    cMessage * msg = new cMessage();
    // Set scheduling priority (smaller numeric value is prioritized)
    // so that mobility is updated before mac.
    msg->setSchedulingPriority(0);
    scheduleAt(simTime() + 1, msg);
}

void Mobility::handleMessage(cMessage *msg) {

    if (lastDistance < this->getDistanceToTarget(targetX, targetY)) {
        // We reached the closest point to target
        this->getNewTarget();
        nodeModule->bubble("Moving toards a new target!");
    }

    // Update current distance to target and update next position
    lastDistance = this->getDistanceToTarget(targetX, targetY);
    posX += incrementX;
    posY += incrementY;
    // Schedule update message
    scheduleAt(simTime() + 1, msg);
}

void Mobility::getNewTarget() {

    string mobilityType = par("type");
    if (mobilityType.compare("1Dwalk") == 0) {

        // Update target point only on X
        targetX = uniform(par("areaMinX").doubleValue(),
                par("areaMaxX").doubleValue());
        targetY = posY;

        speed = uniform(par("minSpeed").doubleValue(),
                par("maxSpeed").doubleValue());

    } else if (mobilityType.compare("2Dwalk") == 0) {

        // Update target point randomly
        targetX = uniform(par("areaMinX").doubleValue(),
                par("areaMaxX").doubleValue());
        targetY = uniform(par("areaMinY").doubleValue(),
                par("areaMaxY").doubleValue());
        speed = uniform(par("minSpeed").doubleValue(),
                par("maxSpeed").doubleValue());

    } else if (mobilityType.compare("predicted") == 0) {

        // Update target point only on X
        targetX = uniform(par("areaMinX").doubleValue(),
                par("areaMaxX").doubleValue());
        targetY = posY;

        speed = uniform(par("minSpeed").doubleValue(),
                par("maxSpeed").doubleValue());

    } else if (mobilityType.compare("scheduled") == 0) {

        if (mobiltyFile.eof()) {
            // No more targets, end simulation:
            nodeModule->bubble("No more tagets in mobility file!");
            endSimulation();
        }

        // Get target point and speed from file
        mobiltyFile >> targetX >> targetY >> speed;

    } else {
        cout << "Dtn4all error: cannot find mobility " << mobilityType << endl;
        exit(1);
    }

    if (par("record").boolValue()) {

        // Write target and speed to file
        recordFile << targetX << " " << targetY << " " << speed << endl;

    }

    // Compute increments
    incrementX = speed * (targetX - posX)
            / sqrt(pow(targetX - posX, 2) + pow(targetY - posY, 2));
    incrementY = speed * (targetY - posY)
            / sqrt(pow(targetX - posX, 2) + pow(targetY - posY, 2));
}

double Mobility::getDistanceToNode(int eid) {

    if (eid == eid_)
        return 0;

    // Get distance to remote node
    cModule *remoteNodeMobility =
            this->getParentModule()->getParentModule()->getSubmodule("node",
                    eid)->getSubmodule("mobility");

    double remoteX = ((Mobility*) remoteNodeMobility)->posX;
    double remoteY = ((Mobility*) remoteNodeMobility)->posY;

    return sqrt(pow(remoteX - posX, 2) + pow(remoteY - posY, 2));
}

double Mobility::getDistanceToTarget(double targetX, double targetY) {
    return sqrt(pow(targetX - posX, 2) + pow(targetY - posY, 2));
}

void Mobility::refreshDisplay() const {
    char buf[40];
    sprintf(buf, "Dist: %4.1f", lastDistance);
    getDisplayString().setTagArg("t", 0, buf);
}

void Mobility::finish() {

    if (par("record").boolValue()) {

        // Close record file
        recordFile.close();
    }

}

Mobility::Mobility() {

}

Mobility::~Mobility() {

}

