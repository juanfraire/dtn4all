/*
 * Record.h
 *
 *  Created on: Apr 3, 2017
 *      Author: jfinochietto
 */

#ifndef NODE_RECORD_H_
#define NODE_RECORD_H_

#include <cstdio>
#include <string>
#include <set>
#include <tuple>

#include <omnetpp.h>
#include <dtn4all_m.h>

using namespace std;
using namespace omnetpp;

class Record {
public:
    Record();
    virtual ~Record();

    void registerBundle(Bundle *bundle);
    bool checkIfBundleRegistered(Bundle *bundle);
    void unregisterBundle(Bundle *bundle);

private:
    // Record of received bundles
    set<tuple<simtime_t, int, int>> bundleIDs; // ID made up of creationTime + sequenceNumber + sourceID

};

#endif /* NODE_RECORD_H_ */
