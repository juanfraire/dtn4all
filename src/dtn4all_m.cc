//
// Generated file, do not edit! Created by nedtool 5.5 from dtn4all.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include "dtn4all_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i=0; i<n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp


// forward
template<typename T, typename A>
std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec);

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// operator<< for std::vector<T>
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');
    
    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

Register_Class(Bundle)

Bundle::Bundle(const char *name, short kind) : ::omnetpp::cPacket(name,kind)
{
    this->sourceEID = 0;
    this->destinationEID = 0;
    this->creationTimestamp = 0;
    this->sequenceNumber = 0;
    this->lifetime = 0;
    this->hopCount = 0;
    this->hopLimit = 0;
    this->custodyTransferRequest = false;
    this->applicationAcknowledgmentRequest = false;
    this->previousNode = 0;
    this->nextHopEid = 0;
    this->maxWaitingTime = 0;
    this->storedTime = 0;
}

Bundle::Bundle(const Bundle& other) : ::omnetpp::cPacket(other)
{
    copy(other);
}

Bundle::~Bundle()
{
}

Bundle& Bundle::operator=(const Bundle& other)
{
    if (this==&other) return *this;
    ::omnetpp::cPacket::operator=(other);
    copy(other);
    return *this;
}

void Bundle::copy(const Bundle& other)
{
    this->sourceEID = other.sourceEID;
    this->destinationEID = other.destinationEID;
    this->creationTimestamp = other.creationTimestamp;
    this->sequenceNumber = other.sequenceNumber;
    this->lifetime = other.lifetime;
    this->hopCount = other.hopCount;
    this->hopLimit = other.hopLimit;
    this->custodyTransferRequest = other.custodyTransferRequest;
    this->applicationAcknowledgmentRequest = other.applicationAcknowledgmentRequest;
    this->previousNode = other.previousNode;
    this->nextHopEid = other.nextHopEid;
    this->maxWaitingTime = other.maxWaitingTime;
    this->storedTime = other.storedTime;
}

void Bundle::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::omnetpp::cPacket::parsimPack(b);
    doParsimPacking(b,this->sourceEID);
    doParsimPacking(b,this->destinationEID);
    doParsimPacking(b,this->creationTimestamp);
    doParsimPacking(b,this->sequenceNumber);
    doParsimPacking(b,this->lifetime);
    doParsimPacking(b,this->hopCount);
    doParsimPacking(b,this->hopLimit);
    doParsimPacking(b,this->custodyTransferRequest);
    doParsimPacking(b,this->applicationAcknowledgmentRequest);
    doParsimPacking(b,this->previousNode);
    doParsimPacking(b,this->nextHopEid);
    doParsimPacking(b,this->maxWaitingTime);
    doParsimPacking(b,this->storedTime);
}

void Bundle::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::omnetpp::cPacket::parsimUnpack(b);
    doParsimUnpacking(b,this->sourceEID);
    doParsimUnpacking(b,this->destinationEID);
    doParsimUnpacking(b,this->creationTimestamp);
    doParsimUnpacking(b,this->sequenceNumber);
    doParsimUnpacking(b,this->lifetime);
    doParsimUnpacking(b,this->hopCount);
    doParsimUnpacking(b,this->hopLimit);
    doParsimUnpacking(b,this->custodyTransferRequest);
    doParsimUnpacking(b,this->applicationAcknowledgmentRequest);
    doParsimUnpacking(b,this->previousNode);
    doParsimUnpacking(b,this->nextHopEid);
    doParsimUnpacking(b,this->maxWaitingTime);
    doParsimUnpacking(b,this->storedTime);
}

int Bundle::getSourceEID() const
{
    return this->sourceEID;
}

void Bundle::setSourceEID(int sourceEID)
{
    this->sourceEID = sourceEID;
}

int Bundle::getDestinationEID() const
{
    return this->destinationEID;
}

void Bundle::setDestinationEID(int destinationEID)
{
    this->destinationEID = destinationEID;
}

::omnetpp::simtime_t Bundle::getCreationTimestamp() const
{
    return this->creationTimestamp;
}

void Bundle::setCreationTimestamp(::omnetpp::simtime_t creationTimestamp)
{
    this->creationTimestamp = creationTimestamp;
}

int Bundle::getSequenceNumber() const
{
    return this->sequenceNumber;
}

void Bundle::setSequenceNumber(int sequenceNumber)
{
    this->sequenceNumber = sequenceNumber;
}

::omnetpp::simtime_t Bundle::getLifetime() const
{
    return this->lifetime;
}

void Bundle::setLifetime(::omnetpp::simtime_t lifetime)
{
    this->lifetime = lifetime;
}

int Bundle::getHopCount() const
{
    return this->hopCount;
}

void Bundle::setHopCount(int hopCount)
{
    this->hopCount = hopCount;
}

int Bundle::getHopLimit() const
{
    return this->hopLimit;
}

void Bundle::setHopLimit(int hopLimit)
{
    this->hopLimit = hopLimit;
}

bool Bundle::getCustodyTransferRequest() const
{
    return this->custodyTransferRequest;
}

void Bundle::setCustodyTransferRequest(bool custodyTransferRequest)
{
    this->custodyTransferRequest = custodyTransferRequest;
}

bool Bundle::getApplicationAcknowledgmentRequest() const
{
    return this->applicationAcknowledgmentRequest;
}

void Bundle::setApplicationAcknowledgmentRequest(bool applicationAcknowledgmentRequest)
{
    this->applicationAcknowledgmentRequest = applicationAcknowledgmentRequest;
}

int Bundle::getPreviousNode() const
{
    return this->previousNode;
}

void Bundle::setPreviousNode(int previousNode)
{
    this->previousNode = previousNode;
}

int Bundle::getNextHopEid() const
{
    return this->nextHopEid;
}

void Bundle::setNextHopEid(int nextHopEid)
{
    this->nextHopEid = nextHopEid;
}

::omnetpp::simtime_t Bundle::getMaxWaitingTime() const
{
    return this->maxWaitingTime;
}

void Bundle::setMaxWaitingTime(::omnetpp::simtime_t maxWaitingTime)
{
    this->maxWaitingTime = maxWaitingTime;
}

::omnetpp::simtime_t Bundle::getStoredTime() const
{
    return this->storedTime;
}

void Bundle::setStoredTime(::omnetpp::simtime_t storedTime)
{
    this->storedTime = storedTime;
}

class BundleDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
  public:
    BundleDescriptor();
    virtual ~BundleDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(BundleDescriptor)

BundleDescriptor::BundleDescriptor() : omnetpp::cClassDescriptor("Bundle", "omnetpp::cPacket")
{
    propertynames = nullptr;
}

BundleDescriptor::~BundleDescriptor()
{
    delete[] propertynames;
}

bool BundleDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<Bundle *>(obj)!=nullptr;
}

const char **BundleDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *BundleDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int BundleDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 13+basedesc->getFieldCount() : 13;
}

unsigned int BundleDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<13) ? fieldTypeFlags[field] : 0;
}

const char *BundleDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldNames[] = {
        "sourceEID",
        "destinationEID",
        "creationTimestamp",
        "sequenceNumber",
        "lifetime",
        "hopCount",
        "hopLimit",
        "custodyTransferRequest",
        "applicationAcknowledgmentRequest",
        "previousNode",
        "nextHopEid",
        "maxWaitingTime",
        "storedTime",
    };
    return (field>=0 && field<13) ? fieldNames[field] : nullptr;
}

int BundleDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount() : 0;
    if (fieldName[0]=='s' && strcmp(fieldName, "sourceEID")==0) return base+0;
    if (fieldName[0]=='d' && strcmp(fieldName, "destinationEID")==0) return base+1;
    if (fieldName[0]=='c' && strcmp(fieldName, "creationTimestamp")==0) return base+2;
    if (fieldName[0]=='s' && strcmp(fieldName, "sequenceNumber")==0) return base+3;
    if (fieldName[0]=='l' && strcmp(fieldName, "lifetime")==0) return base+4;
    if (fieldName[0]=='h' && strcmp(fieldName, "hopCount")==0) return base+5;
    if (fieldName[0]=='h' && strcmp(fieldName, "hopLimit")==0) return base+6;
    if (fieldName[0]=='c' && strcmp(fieldName, "custodyTransferRequest")==0) return base+7;
    if (fieldName[0]=='a' && strcmp(fieldName, "applicationAcknowledgmentRequest")==0) return base+8;
    if (fieldName[0]=='p' && strcmp(fieldName, "previousNode")==0) return base+9;
    if (fieldName[0]=='n' && strcmp(fieldName, "nextHopEid")==0) return base+10;
    if (fieldName[0]=='m' && strcmp(fieldName, "maxWaitingTime")==0) return base+11;
    if (fieldName[0]=='s' && strcmp(fieldName, "storedTime")==0) return base+12;
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *BundleDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "simtime_t",
        "int",
        "simtime_t",
        "int",
        "int",
        "bool",
        "bool",
        "int",
        "int",
        "simtime_t",
        "simtime_t",
    };
    return (field>=0 && field<13) ? fieldTypeStrings[field] : nullptr;
}

const char **BundleDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *BundleDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int BundleDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    Bundle *pp = (Bundle *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *BundleDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    Bundle *pp = (Bundle *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string BundleDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    Bundle *pp = (Bundle *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getSourceEID());
        case 1: return long2string(pp->getDestinationEID());
        case 2: return simtime2string(pp->getCreationTimestamp());
        case 3: return long2string(pp->getSequenceNumber());
        case 4: return simtime2string(pp->getLifetime());
        case 5: return long2string(pp->getHopCount());
        case 6: return long2string(pp->getHopLimit());
        case 7: return bool2string(pp->getCustodyTransferRequest());
        case 8: return bool2string(pp->getApplicationAcknowledgmentRequest());
        case 9: return long2string(pp->getPreviousNode());
        case 10: return long2string(pp->getNextHopEid());
        case 11: return simtime2string(pp->getMaxWaitingTime());
        case 12: return simtime2string(pp->getStoredTime());
        default: return "";
    }
}

bool BundleDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    Bundle *pp = (Bundle *)object; (void)pp;
    switch (field) {
        case 0: pp->setSourceEID(string2long(value)); return true;
        case 1: pp->setDestinationEID(string2long(value)); return true;
        case 2: pp->setCreationTimestamp(string2simtime(value)); return true;
        case 3: pp->setSequenceNumber(string2long(value)); return true;
        case 4: pp->setLifetime(string2simtime(value)); return true;
        case 5: pp->setHopCount(string2long(value)); return true;
        case 6: pp->setHopLimit(string2long(value)); return true;
        case 7: pp->setCustodyTransferRequest(string2bool(value)); return true;
        case 8: pp->setApplicationAcknowledgmentRequest(string2bool(value)); return true;
        case 9: pp->setPreviousNode(string2long(value)); return true;
        case 10: pp->setNextHopEid(string2long(value)); return true;
        case 11: pp->setMaxWaitingTime(string2simtime(value)); return true;
        case 12: pp->setStoredTime(string2simtime(value)); return true;
        default: return false;
    }
}

const char *BundleDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

void *BundleDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    Bundle *pp = (Bundle *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}


