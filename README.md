---------------
Readme
--------------

This repository is for working and using the latest version of DTN4ALL, a simple and pedagogic simulation platform for Delay/Disruption Tolerant Networks. DTN4ALL is expected to be used as part of DTN courses given by Jorge Finochietto (jorge.finochietto@gmail.com) and Juan Fraire (juanfraire@gmail.com). If you have any questions email us!

---------------
Getting started
---------------

DTN4ALL is based in the Omnet++ simulation platform. To get started, download the latest Omnet++ version (currently 5.0) and install it. From the IDE, create a new project using existing git repository and use this one :)